package utils

import (
	models "REST_API/modules/profile/repository/models"
	"sort"
)

//FindIndex for
func FindIndex(profile []models.Profile, id int) int {
	var resp (int) = -1
	for i, p := range profile {
		if p.ID == id {
			resp = i
			break
		}
	}
	return resp
}

//GenID for
func GenID(profile []models.Profile) int {
	if len(profile) == 0 {
		return 1
	}
	var id []int
	for i := range profile {
		id = append(id, profile[i].ID)
	}
	sort.Ints(id)
	return id[len(id)-1] + 1
}
