package utils

import (
	models "REST_API/modules/profile/repository/models"

	"encoding/json"
	"io/ioutil"
	"log"
)

var filename = "./profile_data.json"

//ReadFile is
func ReadFile() ([]models.Profile, error) {
	var profile []models.Profile
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Println(err)
		return profile, err
	}

	err = json.Unmarshal(file, &profile)
	if err != nil {
		log.Println(err)
		return profile, err
	}
	return profile, nil
}

//WriteFile is function write profile to text file
func WriteFile(input []models.Profile) error {
	file, err := json.MarshalIndent(input, "", " ")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(filename, file, 0644)
	return err
}
