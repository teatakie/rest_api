package repository

import (
	utils "REST_API/modules/profile/utils"
	"errors"
)

func (repo *profileRepository) DeleteProfile(id int) error {
	profiles, err := utils.ReadFile()
	if err != nil {
		return err
	}

	index := utils.FindIndex(profiles, id)
	if index == -1 {
		return errors.New("Not found")
	}

	profiles = append(profiles[:index], profiles[index+1:]...)

	err = utils.WriteFile(profiles)
	return err
}
