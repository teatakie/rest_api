package repository

import (
	entity "REST_API/entities"
	models "REST_API/modules/profile/repository/models"
	utils "REST_API/modules/profile/utils"
)

func (repo *profileRepository) CreateProfile(entity *entity.Profile) error {
	profile := new(models.Profile).FromEntityToModel(entity)

	profiles, err := utils.ReadFile()
	if err != nil {
		return err
	}

	newProfile := []models.Profile{{ID: utils.GenID(profiles), Name: profile.Name, Tel: profile.Tel, Email: profile.Email}}
	profiles = append(profiles, newProfile...)

	err = utils.WriteFile(profiles)
	return err
}
