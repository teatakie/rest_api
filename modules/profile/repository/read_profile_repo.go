package repository

import (
	entity "REST_API/entities"
	utils "REST_API/modules/profile/utils"
	"errors"
)

func (repo *profileRepository) ReadProfile(id int) (*entity.Profile, error) {
	profiles, err := utils.ReadFile()
	if err != nil {
		return &entity.Profile{}, err
	}

	index := utils.FindIndex(profiles, id)
	if index == -1 {
		return &entity.Profile{}, errors.New("Not found")
	}

	return profiles[index].FromModelToEntity(), err
}
