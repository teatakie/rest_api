package repository

import (
	entity "REST_API/entities"
	models "REST_API/modules/profile/repository/models"
	utils "REST_API/modules/profile/utils"
	"errors"
)

func (repo *profileRepository) UpdateProfile(entity *entity.Profile) error {
	profile := new(models.Profile).FromEntityToModel(entity)

	profiles, err := utils.ReadFile()
	if err != nil {
		return err
	}

	index := utils.FindIndex(profiles, profile.ID)
	if index == -1 {
		return errors.New("Not found")
	}

	profiles[index].Name = profile.Name
	profiles[index].Tel = profile.Tel
	profiles[index].Email = profile.Email

	return utils.WriteFile(profiles)
}
