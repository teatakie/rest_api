package models

import (
	entity "REST_API/entities"

	"github.com/jinzhu/copier"
)

//Profile is member data
type Profile struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Tel   string `json:"tel"`
	Email string `json:"email"`
}

//FromEntityToModel is function convert entity to model
func (model *Profile) FromEntityToModel(entity *entity.Profile) *Profile {
	profileModel := new(Profile)

	if err := copier.Copy(profileModel, entity); err != nil {
		panic(err)
	}

	return profileModel
}

//FromModelToEntity is function convert model to entity
func (model *Profile) FromModelToEntity() *entity.Profile {
	profileModel := new(entity.Profile)

	if err := copier.Copy(profileModel, model); err != nil {
		panic(err)
	}

	return profileModel
}
