package repository

import (
	entity "REST_API/entities"
	models "REST_API/modules/profile/repository/models"
)

//ProfileRepository is
type ProfileRepository interface {
	ReadProfile(id int) (*entity.Profile, error)
	CreateProfile(entity *entity.Profile) error
	UpdateProfile(entity *entity.Profile) error
	DeleteProfile(id int) error
}

type profileRepository struct {
	Ds models.Profile
}

//NewProfileRepository is
func NewProfileRepository(p models.Profile) ProfileRepository {
	return &profileRepository{
		Ds: p,
	}
}
