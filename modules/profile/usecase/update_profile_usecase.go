package usecase

import (
	entity "REST_API/entities"
)

func (uc *promotionUsecase) UpdateProfile(entity *entity.Profile) error {
	return uc.Repo.UpdateProfile(entity)
}
