package usecase

import (
	entity "REST_API/entities"
	repository "REST_API/modules/profile/repository"
)

//ProfileUsecase is interface
type ProfileUsecase interface {
	ReadProfile(id int) (*entity.Profile, error)
	CreateProfile(entity *entity.Profile) error
	UpdateProfile(entity *entity.Profile) error
	DeleteProfile(id int) error
}

type promotionUsecase struct {
	Repo repository.ProfileRepository
}

//NewProfileUsecase is
func NewProfileUsecase(repo repository.ProfileRepository) ProfileUsecase {
	return &promotionUsecase{
		Repo: repo,
	}
}
