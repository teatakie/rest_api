package usecase

import (
	entity "REST_API/entities"
)

func (uc *promotionUsecase) ReadProfile(id int) (*entity.Profile, error) {
	return uc.Repo.ReadProfile(id)
}
