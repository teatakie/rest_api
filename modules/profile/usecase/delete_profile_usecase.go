package usecase

func (uc *promotionUsecase) DeleteProfile(id int) error {
	return uc.Repo.DeleteProfile(id)
}
