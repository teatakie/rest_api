package usecase

import (
	entity "REST_API/entities"
)

func (uc *promotionUsecase) CreateProfile(entity *entity.Profile) error {
	return uc.Repo.CreateProfile(entity)
}
