package http

import (
	repository "REST_API/modules/profile/repository"
	models "REST_API/modules/profile/repository/models"
	"REST_API/modules/profile/usecase"

	"github.com/gin-gonic/gin"
)

type profileHandle struct {
	UseCase usecase.ProfileUsecase
}

func provideRepository() repository.ProfileRepository {
	return repository.NewProfileRepository(models.Profile{})
}

func provideUseCase() usecase.ProfileUsecase {
	return usecase.NewProfileUsecase(provideRepository())
}

// Route for a function
func Route(r *gin.Engine) {
	v1 := r.Group("/profile")
	{
		h := &profileHandle{
			UseCase: provideUseCase(),
		}

		v1.POST("/", h.CreateProfile)
		v1.GET("/:id", h.ReadProfile)
		v1.PUT("/", h.UpdateProfile)
		v1.DELETE("/:id", h.DeleteProfile)

		v1.POST("", h.CreateProfile)
		v1.PUT("", h.UpdateProfile)
	}
}
