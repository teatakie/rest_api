package http

import (
	incoming "REST_API/modules/profile/delivery/http/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (h *profileHandle) UpdateProfile(c *gin.Context) {
	incomingData := incoming.Profile{}
	if err := c.ShouldBindJSON(&incomingData); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"Status": http.StatusBadRequest, "Detail": err.Error()})
		return
	}

	entity := new(incoming.Profile).ConvertToEntity(incomingData)
	err := h.UseCase.UpdateProfile(entity)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"Status": 20001, "Detail": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"Status": 20000, "Detail": "Success"})
}
