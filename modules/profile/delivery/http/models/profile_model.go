package models

import (
	"REST_API/entities"

	"github.com/jinzhu/copier"
)

//Profile is profile from school
type Profile struct {
	ID     int    `json:"id"`
	Name   string `json:"name"`
	Tel    string `json:"tel"`
	Email  string `json:"email"`
	School string `json:"school"`
}

//ConvertToEntity for convert incoming data from user to entity
func (model *Profile) ConvertToEntity(incoming Profile) *entities.Profile {
	entity := new(entities.Profile)
	err := copier.Copy(entity, incoming)
	if err != nil {
		panic(err)
	}
	return entity
}
