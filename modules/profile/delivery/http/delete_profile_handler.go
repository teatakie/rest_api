package http

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func (h *profileHandle) DeleteProfile(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"Status": http.StatusBadRequest, "Detail": err.Error()})
		return
	}

	err = h.UseCase.DeleteProfile(id)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"Status": 20001, "Detail": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"Status": 20000, "Detail": "Success"})
}
