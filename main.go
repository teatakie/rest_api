package main

import (
	profile "REST_API/modules/profile/delivery/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	profile.Route(r)
	r.Run(":8080")
}
