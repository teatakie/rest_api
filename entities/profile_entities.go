package entities

//Profile is profile from school
type Profile struct {
	ID     int
	Name   string 
	Tel    string 
	Email  string 
	School string 
	Class  string 
	Like   string 
}
